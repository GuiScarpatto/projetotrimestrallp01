object Form1: TForm1
  Left = 162
  Top = 127
  Width = 928
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object EditNome: TEdit
    Left = 184
    Top = 8
    Width = 169
    Height = 21
    TabOrder = 0
    Text = 'Nome'
    OnClick = EditNomeClick
  end
  object EditLoc: TEdit
    Left = 184
    Top = 56
    Width = 169
    Height = 21
    TabOrder = 1
    Text = 'Localiza'#231#227'o'
    OnClick = EditLocClick
  end
  object ListBoxPrincipal: TListBox
    Left = 8
    Top = 8
    Width = 169
    Height = 177
    ItemHeight = 13
    TabOrder = 2
  end
  object ButtonEnviar: TButton
    Left = 184
    Top = 80
    Width = 169
    Height = 25
    Caption = 'Enviar'
    TabOrder = 3
    OnClick = ButtonEnviarClick
  end
  object ButtonApagar: TButton
    Left = 184
    Top = 112
    Width = 169
    Height = 25
    Caption = 'Apagar'
    TabOrder = 4
    OnClick = ButtonApagarClick
  end
  object ButtonAlterar: TButton
    Left = 360
    Top = 80
    Width = 169
    Height = 25
    Caption = 'Alterar'
    TabOrder = 5
    OnClick = ButtonAlterarClick
  end
  object MaskEditCnpj: TMaskEdit
    Left = 184
    Top = 32
    Width = 169
    Height = 21
    EditMask = '000\.000\.000/0000\-99;1;_'
    MaxLength = 19
    TabOrder = 6
    Text = '   .   .   /    -  '
    OnChange = MaskEditCnpjChange
  end
  object EditAlterar: TEdit
    Left = 360
    Top = 8
    Width = 161
    Height = 21
    TabOrder = 7
    Text = 'Alterar Nome'
    OnClick = EditAlterarClick
  end
  object MaskEditAlterar: TMaskEdit
    Left = 360
    Top = 32
    Width = 160
    Height = 21
    EditMask = '000\.000\.000/0000\-99;1;_'
    MaxLength = 19
    TabOrder = 8
    Text = '   .   .   /    -  '
    OnClick = MaskEditAlterarClick
  end
  object EditAlterarLoc: TEdit
    Left = 360
    Top = 56
    Width = 161
    Height = 21
    TabOrder = 9
    Text = 'Alterar Localiza'#231#227'o'
    OnClick = EditAlterarLocClick
  end
end
