unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask;

type
  TForm1 = class(TForm)
    EditNome: TEdit;
    EditLoc: TEdit;
    ListBoxPrincipal: TListBox;
    ButtonEnviar: TButton;
    ButtonApagar: TButton;
    ButtonAlterar: TButton;
    MaskEditCnpj: TMaskEdit;
    EditAlterar: TEdit;
    MaskEditAlterar: TMaskEdit;
    EditAlterarLoc: TEdit;
    procedure EditNomeClick(Sender: TObject);
    procedure EditLocClick(Sender: TObject);
    procedure ButtonApagarClick(Sender: TObject);
    procedure ButtonEnviarClick(Sender: TObject);
    procedure ButtonAlterarClick(Sender: TObject);
    procedure EditAlterarClick(Sender: TObject);
    procedure MaskEditAlterarClick(Sender: TObject);
    procedure EditAlterarLocClick(Sender: TObject);
    procedure MaskEditCnpjChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  Tempresa = class(TObject)
    nome: string;
    cnpj: string;
    localizacao: string;
    end;

var
  Form1: TForm1;
  emp : Tempresa;

implementation

{$R *.dfm}

procedure TForm1.EditNomeClick(Sender: TObject);
begin
     EditNome.clear;
end;

procedure TForm1.EditLocClick(Sender: TObject);
begin
     EditLoc.clear;
end;

procedure TForm1.ButtonApagarClick(Sender: TObject);
begin
     ListBoxPrincipal.DeleteSelected;
end;

procedure TForm1.ButtonEnviarClick(Sender: TObject);
begin
     emp := Tempresa.Create;
     emp.nome := EditNome.Text;
     emp.cnpj := MaskEditCnpj.Text;
     emp.localizacao := EditLoc.Text;
     ListBoxPrincipal.Items.AddObject(emp.nome,emp);
end;


procedure TForm1.ButtonAlterarClick(Sender: TObject);
begin
      if (ListBoxPrincipal.ItemIndex = 0 ) then
      begin
         Form1.ListBoxPrincipal.Items[ListBoxPrincipal.itemindex]:= EditAlterar.text;
         emp.nome := EditAlterar.Text;
         emp.cnpj := MaskEditAlterar.Text;
         emp.localizacao := EditAlterarLoc.Text;
         end;

end;

procedure TForm1.EditAlterarClick(Sender: TObject);
begin
     EditAlterar.clear;
end;

procedure TForm1.MaskEditAlterarClick(Sender: TObject);
begin
     MaskEditAlterar.clear;
end;


procedure TForm1.EditAlterarLocClick(Sender: TObject);
begin
     EditAlterarLoc.Clear;
end;

procedure TForm1.MaskEditCnpjChange(Sender: TObject);
begin
     MaskEditCnpj.Clear;
end;

end.
